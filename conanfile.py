import os
from conans import ConanFile, CMake, tools


class DanRendererConan(ConanFile):
    name = "DanRenderer"
    version = "0.1.0"
    description = "Tiny Software Renderer"
    url = "https://github.com/ssloy/tinyrenderer/wiki"
    homepage = "https://github.com/ssloy/tinyrenderer/wiki"
    author = "Daniel Obermaier"
    topics = ("conan", "header-only", "renderer", "cpp11")
    license = "MIT"
    exports = "LICENSE"
    exports_sources = "CMakeLists.txt"
    generators = "cmake"
    no_copy_source = True

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder=".")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="hello")
        self.copy("*hello.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)


    def package_info(self):
        pass